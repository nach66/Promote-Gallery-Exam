import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import Lightbox from 'lightbox-react';
import 'react-image-lightbox/style.css';
import 'lightbox-react/style.css';
import './Image.scss';



class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };
  constructor(props) {
    super(props);
    this.state = {
      size: 200,
      fil: 0,
      ids: 'img0',
      width: window.innerWidth,
      isOpen: false
    };
    this.calcImageSize = this.calcImageSize.bind(this);
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  //task 2, Responsive
  calcImageSize() {
    const targetSize = 200;
    const imagesPerRow = Math.round(this.state.width / targetSize);

    let space = 3.5;
    if (this.state.width < 900)
      space = 5.5;
    if (this.state.width < 700)
      space = 8.5;

    let imgSize = ((this.state.width / imagesPerRow) - space);
    let percent = (imgSize / this.state.width);
    let wid = this.state.width * percent;
    this.setState({
      size: wid
    });
  }
  updateDimensions() {
    this.setState({
      width: window.innerWidth
    });
    this.calcImageSize();
  }
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.calcImageSize();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }
  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  render() {
    const { isOpen } = this.state;

    return (
      <div
        className="image-root"
        id={this.state.ids}
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px'
        }}
      >
        <div>
          <FontAwesome className="image-icon" name="clone" title="clone"
            onClick={this.handleClickc.bind(this)} />
          <FontAwesome className="image-icon" name="filter" title="filter"
            onClick={() => this.handleClickf()} />
          <FontAwesome className="image-icon" name="expand" title="expand"
            onClick={() => this.handleClicke()} />
          {isOpen && (
            <Lightbox
              mainSrc={this.urlFromDto(this.props.dto)}
              onCloseRequest={() => this.setState({ isOpen: false })}
            />
          )}
          </div>
      </div>
    );
  }

  //task 1, filters
  handleClickf() {
    if (this.state.fil === 5)
      this.setState({ fil: 0 });
    else if (this.state.fil === 0)
      this.setState({ fil: 1 });
    else {
      this.setState({
        fil: this.state.fil + 1
      });
    }
    this.setState({ ids: 'img'+ this.state.fil });
  }
  //task 1, Expand
  handleClicke() {
    this.setState({ isOpen: true });
  }
  //task 1, clone
  handleClickc() {
    this.props.callBack(this.props.dto);
  }
}

export default Image;
