import React from 'react';
import './App.scss';
import Gallery from '../Gallery';
//import ReactDOM from 'react-dom';
import { DelayInput } from 'react-delay-input';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'art'
    };
  }
  
  //task 2, Delay
  render() {
    return (
      <div className="app-root" >
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <DelayInput delayTimeout={500} className="app-input" value={this.state.tag}
            onChange={event => this.setState({ tag: event.target.value })}  />
        </div>
        <Gallery tag={this.state.tag} />
      </div>
    );
  }
}

export default App;
