import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
//import { render } from 'react-dom';
//import InfiniteScroll from "react-infinite-scroll-component";

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth()
    };
    this.trackScrolling = this.trackScrolling.bind(this);
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  getImages(tag) {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          this.setState({ images: res.photos.photo });
        }
      });
  }
  addImages(tag) {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any
&per_page=${100}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          this.setState({ images: this.state.images.concat(res.photos.photo) });
        }
      });
  }

  componentWillReceiveProps(props) {
    this.getImages(props.tag);
  }
  componentDidMount() {
    this.getImages(this.props.tag);
    this.setState({ galleryWidth: document.body.clientWidth });
    window.addEventListener('scroll', this.trackScrolling);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.trackScrolling);
  }

  //task 2, Infinite Scroll
  trackScrolling() {
    if ((window.innerHeight + window.scrollY) >= document.documentElement.scrollHeight) {
      this.addImages(this.props.tag);
    }
  }

  clone_botton(img) {
    this.setState({ images: this.state.images.concat(img) });
  }

  render() {
      return (
        <div className="gallery-root" >
          {
            this.state.images.map( (dto, i) => {
              return <Image key={i} dto={dto}
                callBack={this.clone_botton.bind(this)}
                galleryWidth={this.state.galleryWidth} />;
            })
          }
        </div>
      );
  }
}

export default Gallery;
